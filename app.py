import sys
from equation import configs

sys.path.append(configs.FUNCTIONS_PATH)
sys.path.append(configs.DATA_PATH)

from equation.app import app

if __name__ == '__main__':
    app.run()
