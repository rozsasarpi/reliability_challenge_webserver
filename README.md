### SHEHRYAR ALI
### Software Engineer / Data Scientist
### shehry3894@gmail.com

# ENDPOINTS
[GET] /master-table
```
response:
[
    [
        set_ID,
        problem_ID,
        number_of_inputs,
        maximum_requests_per_user,
        function_name,
        comment
     ],
     ...
]
```

[GET] /auth-table
```
response:

[
    [
        username,
        password
    ],
    ...
]
``` 
[GET] /requests-table

```
response:
 [
    [
        username,
        set_ID,
        problem_ID,
        time_stamp,
        g_val,
        input_list
    ], ...  
 ]
 ```
 
 
 [GET] /requests-count-table

```
response:
 [
    [
        username,
        set_ID,
        problem_ID,
        number_of_requests
    ], ...  
 ]
 ```
 
 
 [POST] /evaluate
 
```
request body:
{
	"username": "user1",
	"password": "password",
	"setID": 1,
	"problemID": 1,
	"inputList": [0,1,2,3,4,5,6,7,8,9]
}

responses:
- {"val": NaN, "msg": "Invalid username/password."}
- {"val": NaN, "msg": "Invalid setID/problemID."}
- {"val": NaN, "msg": "Sorry, Request limit exceeded."}
- {"val": <calculated g_val>, "msg": "Ok"}
```
     