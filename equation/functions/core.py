import os

from flask_jsonpify import jsonpify

from equation import utils


def get_func_files(directory):
    files = os.listdir(directory)
    for file_ in files:
        if file_.endswith('.pyc') or file_.endswith('_'):
            files.remove(file_)
    if '' in files:
        files.remove('')
    if '__init__.py' in files:
        files.remove('__init__.py')
    if 'core.py' in files:
        files.remove('core.py')
    files = [x.replace('.py', '') for x in files]
    return files


imports = get_func_files('./equation/functions/')
print(imports)

modules = {}
for x in imports:
    print(x)
    try:
        modules[x] = __import__(x)
        print("Successfully imported ", x, '.')
    except ImportError:
        print("Error importing ", x, '.')


def exceute_func(function_name, input_list):
    custom_func = 'modules["' + function_name + '"].' + function_name + '(' + str(input_list) + ')'
    return eval(custom_func)


# extract useful fields and process them


def get_table_as_json(filepath):
    print('=== get_table_as_json(filepath) === ', filepath)
    table = utils.get_dataframe(filepath)
    df_list = table.values.tolist()
    json_data = jsonpify(df_list)
    return json_data


def evaluate(function_name, input_list):
    print('=== evaluate(function_name, input_list) ===', function_name, ' | ', input_list)
    val, msg = exceute_func(function_name, input_list)
    return val, msg
