import threading
import time
from equation import utils, configs


class Threading:
    """
    The run() method will be started and it will run in the background
    until the application exits.
    """

    def __init__(self, interval=5):
        """ Constructor
        :type interval: int
        :param interval: Check interval, in seconds
        """
        self.interval = interval
        self.lock = threading.Lock()

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()                                  # Start the execution

    def dump_dataframe(self, df, file_path):
        print('=== dump_dataframe(df, file_path):', file_path)
        self.lock.acquire()
        df.to_csv(file_path, index=False)
        self.lock.release()

    def run(self):
        """ Method that runs forever """
        while True:
            time.sleep(self.interval)
            self.dump_dataframe(utils.request_count_table, configs.REQUESTS_COUNT_TABLE_PATH)
            self.dump_dataframe(utils.request_table, configs.REQUESTS_TABLE_PATH)
