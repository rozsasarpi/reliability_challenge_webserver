import json
import threading
import time
import traceback

import pandas as pd

from equation import configs
from equation.thread_manager import Threading

lock = threading.Lock()


def get_dataframe(file_path):
    print('=== get_dataframe(file_path) ===', file_path)
    return pd.read_csv(file_path)


# init tables
master_table = get_dataframe(configs.MASTER_TABLE_PATH)
auth_table = get_dataframe(configs.AUTH_TABLE_PATH)
request_table = get_dataframe(configs.REQUESTS_TABLE_PATH)
request_count_table = get_dataframe(configs.REQUESTS_COUNT_TABLE_PATH)


def validate_user(username, password):
    print('=== validate_user(username, password) ===', username, ' | ', password)
    global auth_table
    conditional_row = auth_table[(auth_table.username == username) & (auth_table.password == password)]
    if len(conditional_row['username'].values) > 0:
        return True
    else:
        return False


def get_function_name(set_id, problem_id):
    print('=== get_function_name(set_id, problem_id):', set_id, problem_id)
    global master_table
    try:
        conditional_row = master_table[(master_table.set_ID == set_id) & (master_table.problem_ID == problem_id)]
        function_name = conditional_row['function_name'].values[0]
        number_of_inputs = conditional_row['number_of_inputs'].values[0]
        return function_name, number_of_inputs
    except:
        print(traceback.print_exc())
        return False, False


def load_json(json_str):
    try:
        req_json = json.loads(json_str)
        return True, req_json
    except:
        print(traceback.print_exc())
        return False, 'Invalid POST request json body.'


def validate_input(input_list, number_of_inputs):
    print('=== validate_input(input_list, number_of_inputs):', input_list, ' | ', number_of_inputs)
    valid_input, msg = False, 'Invalid input'
    if isinstance(input_list, list):
        if len(input_list) == number_of_inputs:
            valid_input = True
            msg = 'Valid input.'
        elif len(input_list) < number_of_inputs:
            valid_input = False
            msg = 'Less inputs than expected (' + str(number_of_inputs) + ').'
            return valid_input, msg
        else:
            valid_input = False
            msg = 'More inputs than expected (' + str(number_of_inputs) + ').'
            return valid_input, msg

        if all(isinstance(x, float) for x in input_list) or \
                all(isinstance(x, int) for x in input_list):
            valid_input = True
            msg = 'Valid input.'
        else:
            valid_input = False
            msg = 'Input has invalid data types.'
            return valid_input, msg
    else:
        valid_input = False
        msg = 'Incorrect input type, list of floating numbers is expected'
    return valid_input, msg


def get_curr_request_count(username, set_id, problem_id):
    print('=== get_curr_request_count(username, set_id, problem_id):', username, set_id, problem_id)
    global request_count_table
    match_criteria = (request_count_table['username'] == username) & (request_count_table['set_ID'] == set_id) \
        & (request_count_table['problem_ID'] == problem_id)

    res = request_count_table[match_criteria].shape[0]
    if res <= 0:  # if record exists
        request_count_table = request_count_table.append({'username': username, 'set_ID': set_id,
                                                          'problem_ID': problem_id, 'number_of_requests': 0},
                                                         ignore_index=True)
    match_criteria = (request_count_table['username'] == username) & (request_count_table['set_ID'] == set_id) \
        & (request_count_table['problem_ID'] == problem_id)

    row_idx = request_count_table[match_criteria].index.values.astype(int)[0]
    request_count_table.number_of_requests.iloc[[row_idx]] = request_count_table[match_criteria][
                                                                 'number_of_requests'] + 1
    return request_count_table[match_criteria]['number_of_requests'].values[0]


def get_valid_request_count(set_id, problem_id):
    print('=== get_valid_request_count(set_id, problem_id):', set_id, problem_id)
    global master_table
    match_criteria = (master_table['set_ID'] == set_id) & (master_table['problem_ID'] == problem_id)
    return master_table[match_criteria]['maximum_requests_per_user'].values[0]


def validate_request_count(username, set_id, problem_id):
    print('=== validate_request_count(username, set_id, problem_id):', username, set_id, problem_id)
    curr_req_count = get_curr_request_count(username, set_id, problem_id)
    valid_req_count = get_valid_request_count(set_id, problem_id)

    print('curr_req_count, valid_req_count', curr_req_count, valid_req_count)

    if curr_req_count < valid_req_count:
        return True
    return False


def add_request(username, set_id, problem_id, g_val, input_list):
    print('=== add_request(username, set_id, problem_id, g_val, input_list)')
    global request_table
    lock.acquire()
    request_table = request_table.append(
        {'username': username, 'set_ID': set_id, 'problem_ID': problem_id, 'g_val': g_val,
         'time_stamp': time.time(), 'input_list': input_list}, ignore_index=True)
    lock.release()


def dump_dataframe(df, file_path):
    print('=== dump_dataframe(df, file_path):', file_path)
    lock.acquire()
    df.to_csv(file_path, index=False)
    lock.release()


Threading(interval=30)
